
////////////////////////////////////////////////////////////
// Globals
////////////////////////////////////////////////////////////

// Helpers:

const rand = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

// Constants:

const stateIs = { animating: false };
const speedTo = {};
const spacing = 30;

// Elements:

const doc = $(document);
const win = $(window);
const navigation = $(".t-navigation");
const heroBackground = $(".t-hero__background");

// Globals:

var scroll_up = false;
var scroll = win.scrollTop();

win.scroll(() => scroll_up = scroll > win.scrollTop());
win.scroll(() => scroll = win.scrollTop());

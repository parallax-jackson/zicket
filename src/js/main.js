
doc.ready(() => {

	//////////////////////////////////////////////////
	// Navigation
	//////////////////////////////////////////////////

	// Constants:

	const navigationHeight = 80;
	const navigationContrastPoint = win.outerHeight() * .2;

	// Methods:

	const updateNavigationContrast = () => {

		scroll >= navigationContrastPoint
		? navigation.addClass("-withContrast")
		: navigation.removeClass("-withContrast");
	}

	// Events:

	win.scroll(updateNavigationContrast);
	

	//////////////////////////////////////////////////
	// Header
	//////////////////////////////////////////////////

	// Constants:

	const settings = { shift: .25, fade: .8 };

	// Methods:

	const parallax = (element, { shift, fade }) => {

		const percent = scroll / win.outerHeight();

		element.css({

			opacity: `${ 1 - (percent * fade) }`,
			transform: `translate3d(0, ${ scroll * shift }px, 0)`,

		});
	}

	// Events:

	win.scroll(() => parallax(heroBackground, settings));

	// Init:

	parallax(heroBackground, settings);


	//////////////////////////////////////////////////
	// Carousel
	//////////////////////////////////////////////////

	// ...


	//////////////////////////////////////////////////
	// Email Signup:
	//////////////////////////////////////////////////

	// Constants:

	const emailInput = $("#emailInput");

	// Fetch:

	doc.on("click", "#formSubmit", event => {

		event.preventDefault();

		const submission = { email: emailInput.val() }

		fetch("https://zicket-api.bradlc41.now.sh/api/newsletter", {

			method: 'POST',
			body: JSON.stringify(submission),
	        headers: { 'Content-Type': 'application/json' }

		}).then(response => response.json()).then(({ success, errors }) => {

			emailInput.attr("data-validation", success);

		})

	});

});
